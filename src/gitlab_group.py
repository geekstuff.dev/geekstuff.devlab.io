import gitlab, gitlab_project as project, jinja2, os

class GitlabGroup:
    def __init__(self, glClient: gitlab.Gitlab, groupID: int, group_templates: dict[str, jinja2.Template], project_templates: dict[str, jinja2.Template]):
        self.client = glClient
        self.group = glClient.groups.get(groupID)
        self.group_templates = group_templates
        self.project_templates = project_templates

    def generate(self):
        for gp in self.group.projects.list(iterator=True,per_page=50,visibility="public"):
            pid = gp.get_id()
            rgp = self.client.projects.get(pid)
            if "org.publish" not in rgp.__getattr__("topics"):
                continue
            p = project.GitlabProject(rgp, self.project_templates)
            p.write()

        for sg in self.group.subgroups.list(iterator=True,per_page=50,visibility="public"):
            g = GitlabGroup(self.client, sg.id, self.group_templates, self.project_templates)
            g.write()
            g.generate()

    def write(self):
        parent = '/'.join(f"{self.group.full_path}".split("/")[1:])
        dir = f"docs/projects/{parent}"
        self.write_index(dir)
        print(f"docs generated for group {dir}")

    def write_index(self, dir: str):
        filename = f"{dir}/index.md"
        content = self.group_templates["index"].render({
            "avatar_link": self.group.attributes.get("avatar_url"),
            "group": self.group.attributes,
            "short_description": self.group.__getattr__("description"),
            "title_meta": self.group.name,
            "title_with_namespace": "/".join(self.group.full_name.split("/")[1:]),
            "web_url": "[{}]({})".format("{}".format(self.group.attributes.get("web_url")).replace("https://", ""), self.group.attributes.get("web_url"))
        })
        os.makedirs(dir, exist_ok=True)
        with open(filename, mode="w", encoding="utf-8") as message:
            message.write(content)
