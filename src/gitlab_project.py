import jinja2, gitlab.v4.objects.projects as projects, re, os

class GitlabProject:
    def __init__(self, project: projects.Project, templates: dict[str, jinja2.Template]):
        templateDescriptionRegex = r"^template_description_.*\.md$"
        self.templates = templates
        self.project = project
        release = ""
        release_tag = ""
        release_description_url = ""
        release_url = ""
        title_link = "[{}]({})".format("/".join(project.__getattr__("name_with_namespace").split("/")[1:]), project.attributes.get("web_url"))
        avatar_link = project.attributes.get("avatar_url")
        web_url_title = "{}".format(project.attributes.get("web_url")).replace("https://", "")

        for release in project.releases.list(Iterator=True):
            title_link = "[{}]({})".format((" - ".join([project.__getattr__("name_with_namespace").split("/", 1)[1], release.__getattr__("tag_name")])), release.__getattr__("_links")["self"])
            release_tag = release.__getattr__("tag_name")
            release_url = release.__getattr__("_links")["self"]
            break

        if release:
            for link in release.attributes.get("assets")["links"]:
                if re.match(templateDescriptionRegex, link["name"]):
                    release_description_url = link["url"]
                    break

        self.data = {
            "avatar_link": avatar_link,
            "id": project.get_id(),
            "project": project.attributes,
            "tags": [],
            "title_meta": project.name,
            "title_link": title_link,
            "title_with_namespace": "/".join(project.__getattr__("name_with_namespace").split("/")[1:]),
            "readme": project.__getattr__("description"),
            "release_description_url": release_description_url,
            "release_tag": release_tag,
            "release_tag_url": "[{}]({})".format(release_tag, release_url),
            "release_url": release_url,
            "short_description": project.__getattr__("description"),
            "web_url": "[{}]({})".format(web_url_title, project.attributes.get("web_url"))
        }

    def write(self):
        parent = f"{self.project.path_with_namespace}".split("/")[1:-1]
        parent = '/'.join(parent)
        dir = f"docs/projects/{parent}/{self.project.path}"
        self.write_index(dir)
        if self.data["readme"] != "":
            self.write_readme(dir)
        if self.data["release_description_url"] != "":
            self.write_howto(dir)
        self.write_pages(dir)
        print(f"docs generated for project {dir}")

    def write_index(self, dir: str):
        filename = f"{dir}/index.md"
        content = self.templates["index"].render(self.data)
        os.makedirs(dir, exist_ok=True)
        with open(filename, mode="w", encoding="utf-8") as message:
            message.write(content)

    def write_howto(self, dir: str):
        filename = f"{dir}/howto.md"
        content = self.templates["howto"].render(self.data)
        os.makedirs(dir, exist_ok=True)
        with open(filename, mode="w", encoding="utf-8") as message:
            message.write(content)

    def write_readme(self, dir: str):
        filename = f"{dir}/readme.md"
        content = self.templates["readme"].render(self.data)
        os.makedirs(dir, exist_ok=True)
        with open(filename, mode="w", encoding="utf-8") as message:
            message.write(content)

    def write_pages(self, dir: str):
        filename = f"{dir}/.pages"
        content = self.templates["pages"].render(self.data)
        os.makedirs(dir, exist_ok=True)
        with open(filename, mode="w", encoding="utf-8") as message:
            message.write(content)
