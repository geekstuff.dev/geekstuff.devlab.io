#import math
import gitlab
import re

def define_env(env):
    """
    This is the hook for defining variables, macros and filters

    - variables: the dictionary that contains the environment variables
    - macro: a decorator function, to declare a macro.
    - filter: a function with one of more arguments,
        used to perform a transformation
    """

    # # add to the dictionary of variables available to markdown pages:
    # env.variables['baz'] = "John Doe"

    # # NOTE: you may also treat env.variables as a namespace,
    # #       with the dot notation:
    # env.variables.baz = "John Doe"

    # @env.macro
    # def bar(x):
    #     return (2.3 * x) + 7

    # # If you wish, you can  declare a macro with a different name:
    # def f(x):
    #     return x * x
    # env.macro(f, 'barbaz')

    # # or to export some predefined function
    # env.macro(math.floor) # will be exported as 'floor'

    # # create a jinja2 filter
    # @env.filter
    # def reverse(x):
    #     "Reverse a string (and uppercase)"
    #     return x.upper()[::-1]

    templateDescriptionRegex = r"^template_description_v.*\.md$"

    @env.macro
    def gitlab_project_latest_release(id):
        gl = gitlab.Gitlab()
        project = gl.projects.get(id)
        for release in project.releases.list(Iterator=True):
            break

        return "[{} - {}]({})".format(project.__getattr__("name_with_namespace").split("/", 1)[1], release.__getattr__("tag_name"), release.__getattr__("_links")["self"])

    @env.macro
    def gitlab_project_latest_release_description(id):
        gl = gitlab.Gitlab()
        project = gl.projects.get(id)
        for release in project.releases.list(Iterator=True):
            break

        for link in release.attributes.get("assets")["links"]:
            if re.match(templateDescriptionRegex, link["name"]):
                return link["url"]

        return "not found"
