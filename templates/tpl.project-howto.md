[#- templates/tpl.howto.md -#]

---
title: [[ title_with_namespace ]] - How to use
description: >-
  [[ short_description ]]

[=- if tags =]
tags:
  [=- for tag in tags =]
  - [[ tag ]]
  [=- endfor =]
[= else =]
tags: []
[=- endif =]
---

# [[ title_with_namespace ]] - How to use

The following info was pulled from latest release [[ release_tag_url ]].

[=- if release_description_url =]
--8<-- "[[ release_description_url ]]"
[= else =]
  Unavailable
[=- endif =]
