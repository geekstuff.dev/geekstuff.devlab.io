[#- templates/tpl.project.md -#]

---
title: [[ title_with_namespace ]]
description: >-
  [[ short_description ]]

[=- if tags =]
tags:
  [=- for tag in tags =]
  - [[ tag ]]
  [=- endfor =]
[= else =]
tags: []
[=- endif =]
---

# [[ title_with_namespace ]]

[=- if avatar_link =]
![Logo]([[ avatar_link ]]){ align=left loading=lazy style="height:100px" }
[=- endif =]

[[ short_description ]]
<div style="clear:both;height:1px"></div>

!!! note "Links"
    [= if release_description_url -=]
    **[How to use](./howto.md)**
    [=- endif =]

    [= if readme -=]
    **[README](./readme.md)**
    [=- endif =]

    Source code: **[[ web_url ]]**

    [= if release_tag -=]
    Latest release: **[[ release_tag_url ]]**
    [=- endif =]

    Clone over HTTPS:
    ```
    git clone [[ project.http_url_to_repo ]]
    ```

    Clone over SSH:
    ```
    git clone [[ project.ssh_url_to_repo ]]
    ```
