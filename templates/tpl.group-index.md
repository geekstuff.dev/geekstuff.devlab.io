---
title: [[ title_with_namespace ]]
description: >-
  [[ short_description ]]

[=- if tags =]
tags:
  [=- for tag in tags =]
  - [[ tag ]]
  [=- endfor =]
[= else =]
tags: []
[=- endif =]
---

# [[ title_with_namespace ]]

[=- if avatar_link =]
![Logo]([[ avatar_link ]]){ align=left loading=lazy style="height:100px" }
[=- endif =]

[[ short_description ]]

**[[ web_url ]]**
