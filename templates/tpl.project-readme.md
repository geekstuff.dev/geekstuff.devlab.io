[#- templates/tpl.readme.md -#]

---
title: [[ title_with_namespace ]] - Readme
description: >-
  [[ short_description ]]

[=- if tags =]
tags:
  [=- for tag in tags =]
  - [[ tag ]]
  [=- endfor =]
[= else =]
tags: []
[=- endif =]
---

[=- if project.readme_url =]
--8<-- "[[ project.readme_url|replace('-/blob/', '-/raw/') ]]"
[= else =]
  Unavailable
[=- endif =]
