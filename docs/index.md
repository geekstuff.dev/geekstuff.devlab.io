---
hide:
  - navigation
  - toc
---

# Geekstuff.Dev

This site contains information and generated content on git projects, libraries,
starting kits, templates and examples around IT technologies, services, pipelines,
apps, infra, infra-as-code, automation, all environments, containers, kubernetes,
old techno, new techno, event-driven, new ways, etc.
