#!/usr/bin/env python3

import sys
sys.path.append('scripts/')
sys.path.append('src/')

import gitlab, gitlab_group as gp, os
from jinja2 import Environment, FileSystemLoader

generated_dir = "docs/projects"

def cleanup_destination():
    for root, dirs, files in os.walk(generated_dir, topdown=False):
        for name in files:
            fullpath = os.path.join(root, name)
            if name in [".gitignore"]:
                continue
            if fullpath in ["docs/projects/index.md", "docs/projects/.pages"]:
                continue
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))

def generate_files():
    cleanup_destination()

    environment = Environment(
        loader=FileSystemLoader("templates/"),
        block_start_string='[=',
        block_end_string='=]',
        comment_start_string='[#',
        comment_end_string='#]',
        variable_start_string='[[',
        variable_end_string=']]',
    )

    project_templates = {
        "index": environment.get_template("tpl.project-index.md"),
        "pages": environment.get_template("tpl.project-pages"),
        "readme": environment.get_template("tpl.project-readme.md"),
        "howto": environment.get_template("tpl.project-howto.md"),
    }

    group_templates = {
        "index": environment.get_template("tpl.group-index.md"),
    }

    client = gitlab.Gitlab()
    group = gp.GitlabGroup(client, 59943467, group_templates, project_templates)
    group.generate()

# Run if script is called directly
if __name__ == '__main__':
    generate_files()

def on_startup(command, dirty):
    generate_files()
