#!/bin/sh

set -e

if pgrep -f "/usr/local/bin/python /usr/local/bin/mkdocs"; then
    echo "Already running"
    exit 1
fi

mkdocs serve --dev-addr=127.0.0.1:8000 \
    -w main.py
