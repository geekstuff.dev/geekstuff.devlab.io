# control squidfunk version via docker-compose.yml
ARG SQUIDFUNK_MKDOCS_MATERIAL_VERSION

# Same image used in horizon
FROM squidfunk/mkdocs-material:${SQUIDFUNK_MKDOCS_MATERIAL_VERSION}

# User customizable values
ARG GIT_EMAIL
ARG GIT_SIGNINGKEY

# Copy devcontainer scripts
COPY --from=geekstuffreal/devcontainer:v0.14 /scripts/ /devcontainer/

# Setup devcontainer
RUN GIT_EMAIL=${GIT_EMAIL} \
    GIT_SIGNINGKEY=${GIT_SIGNINGKEY} \
        /devcontainer/basics.sh \
    && /devcontainer/tools/direnv.sh \
    && /devcontainer/user/starship.sh

COPY ./requirements.txt ./
RUN pip3 --quiet --disable-pip-version-check install -r requirements.txt

# Base image defines entrypoint but in devcontainer VSCode needs to use it instead.
ENTRYPOINT []
